package com.example.demoSpring.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class BookEntity {

    @Id
    @GeneratedValue

    private Long id;
    private String name;
    private String description;


}
