package com.example.demoSpring.bootstrap;

import com.example.demoSpring.entity.BookEntity;
import com.example.demoSpring.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BootStrapData implements CommandLineRunner {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {
        BookEntity book = BookEntity.builder()
                .name("Book1")
                .description("Description1")
                .build();

        bookRepository.save(book);

        log.info("Books count in DB:{}", bookRepository.count());
    }
}
