package com.example.demoSpring.service;

import com.example.demoSpring.entity.BookEntity;
import com.example.demoSpring.repository.BookRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public class BookService {

    public void saveBook(BookEntity bookEntity) {
        //todo
        authorService.saveAuthor(bookEntity);
        //save book
    }

    public BookService(AuthorService authorService, BookRepository bookRepository) {
        this.authorService = authorService;
        this.bookRepository = bookRepository;
    }

    private AuthorService authorService;
    private BookRepository bookRepository ;
}