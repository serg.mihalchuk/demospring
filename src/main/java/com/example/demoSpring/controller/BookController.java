package com.example.demoSpring.controller;

import com.example.demoSpring.entity.BookEntity;
import com.example.demoSpring.repository.BookRepository;
import com.example.demoSpring.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @GetMapping("/books")
    public String test(){
        List<BookEntity> books = bookRepository.findAll();
        return "Hello world";
    }
}
